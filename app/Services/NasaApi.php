<?php
namespace App\Services;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class NasaApi
{
    private $client;
    private $response;
    private $date_apolo;

    //http://docs.guzzlephp.org/en/stable/quickstart.html#sending-requests

    public function __construct()
    {
        // Создать клиента с базовым URI
        $this->client = new \GuzzleHttp\Client(['base_uri' =>'https://api.nasa.gov/']);
        
    }

    public function getDataApiApod(){
        try {
            $this->response =  $this->client->request('GET', 'planetary/apod', ['query' =>
            ['date' => $this->date_apolo,'api_key'=>env('NASA_KEY')]
        ]);
       
        }catch (RequestException $e) {
           echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }

    public function getDataApiEpic(){
        try {
            $this->response =  $this->client->request('GET', 'EPIC/api/natural/images/', ['query' =>
            ['api_key'=>env('NASA_KEY')]
        ]);       
        }catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
    }




    public function setDate($date_apolo=null)
    {
       
        if($date_apolo==null){
           $this->date_apolo = date('Y-m-d');
        } else{
            $this->date_apolo = $date_apolo;
        }
        
    }

    public function apiGetBody()
    {
       return $this->response->getBody();
    }

    /**
     * отримати статус відповіді 200-ок
     *  @return void
     */

    public function apiGetStatusCode()
    {
      return $this->response->getStatusCode();
    }

    /**
     * отримати тип контенту    
     *  @return void
     */


    public function apiGetHeaderLine()
    {
       
       // 'application/json; 

        return $this->response->getHeaderLine('content-type');
    }

    /**
     * Ви можете перевірити поточний ліміт швидкості та деталі 
     * використання, ознайомившись із заголовками X-RateLimit-Limit
     *  @return void
     */

    public function apiGetRateLimit()
    {
        return $this->response->getHeaderLine('X-RateLimit-Limit');
    }

    /**
     * повертаються у кожному відповіді API. Наприклад, якщо API 
     * має погодинний ліміт за замовчуванням 1000 запитів, 
     * зробивши 2 запити, ви отримаєте цей HTTP-заголовок у відповіді на другий запит:
     *  @return void
     */

    public function apiGetRateRemaining()
    {
        return $this->response->getHeaderLine('X-RateLimit-Remaining');
    }


}