<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class NasaApiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('\App\Services\NasaApi', function ($app) {
            return new NasaApi();
          });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
