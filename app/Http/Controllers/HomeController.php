<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\NasaApi;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(NasaApi $customServiceInstance)
    {
        echo env('APP_URL');
        echo $customServiceInstance->doSomethingUseful();
       // return view('home');
    }
}
