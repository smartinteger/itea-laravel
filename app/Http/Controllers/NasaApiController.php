<?php

namespace App\Http\Controllers;

use App\Models\NasaApi as Nasa;
use App\Services\NasaApi;
use Illuminate\Http\Request;

class NasaApiController extends Controller
{
    /**
      * Create a new controller instance.
      *
      * @return void
      */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(NasaApi $customServiceInstance)
    {
        $customServiceInstance->setDate();
        $customServiceInstance->getDataApiApod();
        $request_dada_api = json_decode($customServiceInstance->apiGetBody(), true);
        
        // $post_all =  Nasa::where('date', $request_dada_api['date'])->get();

        $post_ =  Nasa::where('date', date('Y-m-d'))->first();

        if (!$post_) {
            $post_all =  new Nasa();
            $post_all->url = $request_dada_api['hdurl'];
            $post_all->media_type = $request_dada_api['media_type'];
            $post_all->explanation = $request_dada_api['explanation'];
            $post_all->date = $request_dada_api['date'];
            $post_all ->save();
        } else {
            $request_dada_api['hdurl'] = $post_->url;
            $request_dada_api['media_type'] = $post_->media_type;
            $request_dada_api['explanation'] = $post_->explanation;
            $request_dada_api['date'] = $post_->date;
        }

        return view('nasa/media')->withData($request_dada_api)->withTitle('Nasa Media');
    }

    public function set(NasaApi $customServiceInstance, Request $request)
    {
        //dd($request->all());

        $date=$request->input('date');
        $post_ =  Nasa::where('date', $date)->first();

        if (!$post_) {
            $customServiceInstance->setDate($date);
            $customServiceInstance->getDataApiApod();
            $request_dada_api = json_decode($customServiceInstance->apiGetBody(), true);

            $post_all =  new Nasa();
            $post_all->url = $request_dada_api['hdurl'];
            $post_all->media_type = $request_dada_api['media_type'];
            $post_all->explanation = $request_dada_api['explanation'];
            $post_all->date = $request_dada_api['date'];
            $post_all ->save();

        } else {
            $request_dada_api['hdurl'] = $post_->url;
            $request_dada_api['media_type'] = $post_->media_type;
            $request_dada_api['explanation'] = $post_->explanation;
            $request_dada_api['date'] = $post_->date;
        }

       
     
        return view('nasa/media')->withData($request_dada_api)->withTitle('Nasa Media');
    }
/**
 * Undocumented function
 *
 * @param NasaApi $customServiceInstance
 * 
 * @return void
 */
    public function epic(NasaApi $customServiceInstance)
    {
       
        $customServiceInstance->getDataApiEpic();
        $request_dada_api = json_decode($customServiceInstance->apiGetBody(), true);
        print_r( $request_dada_api);
    }
}
