@extends('layouts.app')

@section('title')

{{$title}}

@endsection

@section('content')

<form action="/api-post" method="post">
    @csrf
    <div class="form-group">
    <input required="required" value="" placeholder="Введите название" min="2000-01-02" max={{date('Y-m-d')}} type="date" name = "date" class="form-control" />
    </div>
   
    <input type="submit" name='save' class="btn btn-primary" value = "отправить"/>

  </form>

{{$data['date']}}

{{$data['explanation']}}

@if($data['media_type']=='image')
<a href={{$data['hdurl']}}>{{$data['hdurl']}}</a>
      <p><img width="800" height="1200" src={{$data['hdurl']}}></p>
@else

   <video width="400" height="300">
        <source src={{$data['hdurl']}}>
   </video>
@endif

@endsection
